<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
</head>
<body>
<ul>
    <li>
        <h3>Test 1: Xml parsing</h3>
        <div><a href="https://blueoceantest.herokuapp.com/test1_xml" target="_blank">https://blueoceantest.herokuapp.com/test1_xml</a></div>
    </li>
    <li>
        <h3>Test 2: Soap usage</h3>
        <div><a href="https://blueoceantest.herokuapp.com/test2_soap" target="_blank">https://blueoceantest.herokuapp.com/test2_soap</a></div>
    </li>
    <li>
        <h3>Test 3: MVC test</h3>
        <div><a href="https://blueoceantest.herokuapp.com/test3_MVC" target="_blank">https://blueoceantest.herokuapp.com/test3_MVC</a></div>
    </li>
</ul>
</body>
</html>
