<?php

    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get(
        '/',
        function () {
            return view('index');
        }
    );

    Route::get('/test1_xml', [App\Http\Controllers\Test1XmlController::class, 'index']);
    Route::get('/test2_soap', [App\Http\Controllers\Test2SoapController::class, 'index']);
    Route::get('/test3_MVC', [App\Http\Controllers\View3dController::class, 'index']);
