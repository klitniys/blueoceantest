<h1>BlueOcean</h1>
<h2>PHP experience test</h2>
<div>by Klitniy Sergiy</div>
<div>klitniys@mydigicode.com</div>
<ul>
    <li>
        <h3>Test 1: Xml parsing</h3>
        <div><a href="https://gitlab.com/klitniys/blueoceantest/-/blob/master/app/Http/Controllers/Test1XmlController.php" target="_blank">app/Http/Controllers/Test1XmlController.php</a></div>
        <div>The result is available at the link: <a href="https://blueoceantest.herokuapp.com/test1_xml" target="_blank">https://blueoceantest.herokuapp.com/test1_xml</a></div>
    </li>
    <li>
        <h3>Test 2: Soap usage</h3>
        <div><a href="https://gitlab.com/klitniys/blueoceantest/-/blob/master/app/Http/Controllers/Test2SoapController.php" target="_blank">app/Http/Controllers/Test2SoapController.php</a></div>
        <div>The result is available at the link: <a href="https://blueoceantest.herokuapp.com/test2_soap" target="_blank">https://blueoceantest.herokuapp.com/test2_soap</a></div>
    </li>
    <li>
        <h3>Test 3: MVC test</h3>
        <div><a href="https://gitlab.com/klitniys/blueoceantest/-/blob/master/app/Http/Controllers/View3dController.php" target="_blank">app/Http/Controllers/View3dController.php</a></div>
        <div><a href="https://gitlab.com/klitniys/blueoceantest/-/blob/master/app/Models/View3d.php" target="_blank">app/Models/View3d.php</a></div>
        <div><a href="https://gitlab.com/klitniys/blueoceantest/-/blob/master/resources/views/view3d.blade.php" target="_blank">resources/views/view3d.blade.php</a></div>
        <div>The result is available at the link: <a href="https://blueoceantest.herokuapp.com/test3_MVC" target="_blank">https://blueoceantest.herokuapp.com/test3_MVC</a></div>
    </li>
</ul>
