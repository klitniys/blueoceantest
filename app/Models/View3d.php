<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Class View3d
     * @package App\Models
     */
    class View3d extends Model
    {
        use HasFactory;

        /**
         * @var string
         */
        protected $table = 'view_3d';
    }
