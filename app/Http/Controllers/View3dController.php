<?php

    namespace App\Http\Controllers;

    use App\Models\View3d;

    /**
     * Class View3dController
     * @package App\Http\Controllers
     */
    class View3dController extends Controller
    {
        /**
         * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
         */
        public function index()
        {
            $data = View3d::all()
                ->toArray();

            return view('view3d', compact('data'));
        }
    }
