<?php

    $server_url = 'https://www.crcind.com/csp/samples/SOAP.Demo.CLS?WSDL';

    try {
        $client = new SoapClient($server_url, ["trace" => 1, "exception" => 0]);
    } catch (SoapFault $e) {
        die($e->getMessage());
    }

    $result = $client->GetByName(['name' => 'a']);

    if (!property_exists($result, 'GetByNameResult')) {
        die('response is incorrect');
    }

    $xml = simplexml_load_string($result->GetByNameResult->any);

    if (empty($xml)) {
        die('response is incorrect');
    }

    try {
        $response_array = json_decode(json_encode($xml, JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR);
    } catch (JsonException $e) {
        die($e->getMessage());
    }

    echo "People whose names begin with the letter \"a\": <br />";

    echo implode('<br />', array_column($response_array['ListByName']['SQL'], 'Name'));
